book.name := parkinson,cyril_northcote__parkinsons_law_1
src := src/parkinsons_law_1

book.id := $(shell echo "$(book.name)" | sha1sum | head -c 7)
out := _out
book := $(out)/$(book.id)
book.epub := $(out)/$(book.name).epub
book.mobi := $(out)/$(book.name).mobi
cache := $(out)/cache/$(book.id)

all:

svg.src := $(wildcard $(src)/body/*.jpg $(src)/front/*.jpg)
mathjax.src := $(wildcard $(src)/body/*.mathjax)
svg.dest := $(patsubst $(src)/%.jpg, $(book)/%.svg, $(svg.src)) \
	$(patsubst $(src)/%.mathjax, $(book)/%.svg, $(mathjax.src))
res := $(svg.dest)

$(book)/%.svg: $(src)/%.jpg
	$(mkdir)
	convert $< $(book)/$*.pgm
	potrace -s -O 10 -u 1 -k 0.8 -W 8 -o $@.tmp $(book)/$*.pgm
	echo '<?xml version="1.0" encoding="UTF-8"?>' > $@
	sed 1,3d < $@.tmp >> $@
	rm $(book)/$*.pgm $@.tmp

$(book)/%.svg: $(src)/%.mathjax
	$(mkdir)
	cat $< | xargs -0 tex2svg > $@



xhtml.src := $(shell find $(src) -type f -name '*.xhtml')
xhtml.dest := $(patsubst $(src)/%.xhtml, $(book)/%.xhtml, $(xhtml.src))
res += $(xhtml.dest)

$(book)/%.xhtml: $(src)/%.xhtml $(src)/meta.yaml
	$(mkdir)
	./chapter $(src)/meta.yaml $< $@
# upd chapter deps
	@mkdir -p $(cache)/$(dir $*)
	./chapter_deps '$(src)/%.xhtml' '$(book)/%.xhtml' $< > $(cache)/$*.d

$(cache)/%.d: $(src)/%.xhtml
	$(mkdir)
	./chapter_deps '$(src)/%.xhtml' '$(book)/%.xhtml' $< > $@

xhtml.deps := $(patsubst $(src)/%.xhtml, $(cache)/%.d, $(xhtml.src))
$(foreach d,$(xhtml.deps),$(eval include $$(d)))



$(book)/cover.png: $(src)/cover.svg
	inkscape $< -w 1600 -h 2560 -o $@
res += $(book)/cover.png

static.src := $(shell find $(src) -type f -name '*.css' -o -name '*.xml') \
	$(wildcard $(src)/body/*.svg)
static.dest := $(patsubst $(src)/%, $(book)/%, $(static.src))
res += $(static.dest)

$(book)/%: $(src)/%
	$(mkdir)
	cp $< $@



sections = $(subst $(book)/,, $(sort $(filter $(book)/$1/%, $(xhtml.dest))))
$(book)/nav.xhtml: nav.erb $(src)/meta.yaml $(xhtml.dest)
	cd $(book) && "$(CURDIR)/meta" "$(CURDIR)/$(src)/meta.yaml" "$(CURDIR)/$<" $(call sections,front) $(call sections,body) > "$(CURDIR)/$@"
res += $(book)/nav.xhtml

$(book)/content.opf: content.erb $(res)
	cd $(book) && find front body -type f | xargs "$(CURDIR)/opf" "$(CURDIR)/$(src)/meta.yaml" "$(CURDIR)/$<" style.css > $(notdir $@)
res += $(book)/content.opf



$(book.epub): $(res)
	rm -f $@
	zip -X0 -j $@.tmp $(src)/mimetype
	cd $(book) && zip -r "$(CURDIR)/$@.tmp" *
	epub-hyphen $@.tmp -o $@
	@rm $@.tmp

mobi: $(book.mobi)
%.mobi: %.epub; -kindlegen $<

res: $(res)
epub: $(book.epub)
all: mobi

mkdir = @mkdir -p $(dir $@)
.DELETE_ON_ERROR:
