<?xml version="1.0" encoding="UTF-8"?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title/>
  <link rel="stylesheet" href="../style.css" type="text/css" />
</head>
<body>

<h1 class="title">Parkinson’s Law, or The Rising Pyramid</h1>
  
<p><span class="dropcap">Work expands</span> so as to fill the time available for its completion.

General recognition of this fact is shown in the proverbial phrase “It is the busiest man who has time to spare.” Thus, an elderly lady of leisure can spend the entire day in writing and dispatching a postcard to her niece at Bognor Regis. An hour will be spent in finding the postcard, another in hunting for spectacles, half an hour in a search for the address, an hour and a quarter in composition, and twenty minutes in deciding whether or not to take an umbrella when going to the mailbox in the next street. The total effort that would occupy a busy man for three minutes all told may in this fashion leave another person prostrate after a day of doubt, anxiety, and toil.</p>

<p>Granted that work (and especially paperwork) is thus elastic in its demands on time, it is manifest that there need be little or no relationship between the work to be done and the size of the staff to which it may be assigned. A lack of real activity does not, of necessity, result in leisure.</p>

<p>A lack of occupation is not necessarily revealed by a manifest idleness. The thing to be done swells in importance and complexity in a direct ratio with the time to be spent. This fact is widely recognized, but less attention has been paid to its wider implications, more especially in the field of public administration. Politicians and taxpayers have assumed (with occasional phases of doubt) that a rising total in the number of civil servants must reflect a growing volume of work to be done.

<img alt="" src="01.a.svg" style="float: left" />

Cynics, in questioning this belief, have imagined that the multiplication of officials must have left some of them idle or all of them able to work for shorter hours. But this is a matter in which faith and doubt seem equally misplaced. The fact is that the number of the officials and the quantity of the work are not related to each other at all. The rise in the total of those employed is governed by Parkinson’s Law and would be much the same whether the volume of the work were to increase, diminish, or even disappear. The importance of Parkinson’s Law lies in the fact that it is a law of growth based upon an analysis of the factors by which that growth is controlled.</p>

<p>The validity of this recently discovered law must rest mainly on statistical proofs, which will follow. Of more interest to the general reader is the explanation of the factors underlying the general tendency to which this law gives definition. Omitting technicalities (which are numerous) we may distinguish at the outset two motive forces. They can be represented for the present purpose by two almost axiomatic statements, thus: (1) “An official wants to multiply subordinates, not rivals” and (2) “Officials make work for each other.”</p>

<p>To comprehend Factor 1, we must picture a civil servant, called A, who finds himself overworked. Whether this overwork is real or imaginary is immaterial, but we should observe, in passing, that A’s sensation (or illusion) might easily result from his own decreasing energy: a normal symptom of middle age. For this real or imagined overwork there are, broadly speaking, three possible remedies. He may resign; he may ask to halve the work with a colleague called B; he may demand the assistance of two subordinates, to be called C and D. There is probably no instance in history, however, of A choosing any but the third alternative. By resignation he would lose his pension rights. By having B appointed, on his own level in the hierarchy, he would merely bring in a rival for promotion to W’s vacancy when W (at long last) retires. So A would rather have C and D, junior men, below him. They will add to his consequence and, by dividing the work into two categories, as between C and D, he will have the merit of being the only man who comprehends them both. It is essential to realize at this point that C and D are, as it were, inseparable. To appoint C alone would have been impossible. Why? Because C, if by himself, would divide the work with A and so assume almost the equal status that has been refused in the first instance to B; a status the more emphasized if C is A’s only possible successor. Subordinates must thus number two or more, each being thus kept in order by fear of the other’s promotion. When C complains in turn of being overworked (as he certainly will) A will, with the concurrence of C, advise the appointment of two assistants to help C. But he can then avert internal friction only by advising the appointment of two more assistants to help D, whose position is much the same. With this recruitment of E, F, G, and H the promotion of A is now practically certain.</p>

<p>Seven officials are now doing what one did before. This is where Factor 2 comes into operation. For these seven make so much work for each other that all are fully occupied and A is actually working harder than ever. An incoming document may well come before each of them in turn. Official E decides that it falls within the province of F, who places a draft reply before C, who amends it drastically before consulting D, who asks G to deal with it. But G goes on leave at this point, handing the file over to H, who drafts a minute that is signed by D and returned to C, who revises his draft accordingly and lays the new version before A.</p>

<p>What does A do? He would have every excuse for signing the thing unread, for he has many other matters on his mind. Knowing now that he is to succeed W next year, he has to decide whether C or D should succeed to his own office. He had to agree to G’s going on leave even if not yet strictly entitled to it. He is worried whether H should not have gone instead, for reasons of health. He has looked pale recently--partly but not solely because of his domestic troubles. Then there is the business of F’s special increment of salary for the period of the conference and E’s application for transfer to the Ministry of Pensions. A has heard that D is in love with a married typist and that G and F are no longer on speaking terms--no one seems to know why. So A might be tempted to sign C’s draft and have done with it. But A is a conscientious man. Beset as he is with problems created by his colleagues for themselves and for him--created by the mere fact of these officials’ existence--he is not the man to shirk his duty. He reads through the draft with care, deletes the fussy paragraphs added by C and H, and restores the thing back to the form preferred in the first instance by the able (if quarrelsome) F. He corrects the English--none of these young men can write grammatically--and finally produces the same reply he would have written if officials C to H had never been born. Far more people have taken far longer to produce the same result. No one has been idle. All have done their best. And it is late in the evening before A finally quits his office and begins the return journey to Ealing. The last of the office lights are being turned off in the gathering dusk that marks the end of another day’s administrative toil. Among the last to leave, A reflects with bowed shoulders and a wry smile that late hours, like gray hairs, are among the penalties of success.</p>

<p>From this description of the factors at work the student of political science will recognize that administrators are more or less bound to multiply. Nothing has yet been said, however, about the period of time likely to elapse between the date of A’s appointment and the date from which we can calculate the pensionable service of H. Vast masses of statistical evidence have been collected and it is from a study of this data that Parkinson’s Law has been deduced. Space will not allow of detailed analysis but the reader will be interested to know that research began in the British Navy Estimates. These were chosen because the Admiralty’s responsibilities are more easily measurable than those of, say, the Board of Trade. The question is merely one of numbers and tonnage. Here are some typical figures. The Strength of the Navy in 1914 could be shown as 146,000 officers and men, 3249 dockyard officials and clerks, and 57,000 dockyard workmen. By 1928 there were only 100,000 officers and men and only 62,439 workmen, but the dockyard officials and clerks by then numbered 4558. As for warships, the strength in 1928 was a mere fraction of what it had been in 1914--fewer than 20 capital ships in commission as compared with 62. Over the same period the Admiralty officials had increased in number from 2000 to 3569, providing (as was remarked) “a magnificent navy on land.” These figures are more clearly set forth in tabular form.</p>

<table>
  <caption>Admiralty statistics</caption>
  <tr>
    <th>Year</th>
    <th>Capital ships in commission</th>
    <th>Officers and men in R.N.</th>
    <th>Dockyard workers</th>
    <th>Dockyard officials and clerks</th>
    <th>Admiralty officials</th>
  </tr>
  <tr>
    <td>1914</td>
    <td>62</td>
    <td>146,000</td>
    <td>57,000</td>
    <td>3249</td>
    <td>2000</td>
  </tr>
  <tr>
    <td>1928</td>
    <td>20</td>
    <td>100,000</td>
    <td>62,439</td>
    <td>4558</td>
    <td>3569</td>
  </tr>
  <tr>
    <th>Increase or Decrease</th>
    <td>-67.74%</td>
    <td>-31.5%</td>
    <td>+9.54%</td>
    <td>+40.28%</td>
    <td>+78.45%</td>
  </tr>
</table>

<p>The criticism voiced at the time centered on the ratio between the numbers of those available for fighting and those available only for administration. But that comparison is not to the present purpose. What we have to note is that the 2000 officials of 1914 had become the 3569 of 1928; and that this growth was unrelated to any possible increase in their work.</p>

<p>The Navy during that period had diminished, in point of fact, by a third in men and two-thirds in ships. Nor, from 1922 onward, was its strength even expected to increase; for its total of ships (unlike its total of officials) was limited by the Washington Naval Agreement of that year. Here we have then a 78 per cent increase over a period of fourteen years; an average of 5.6 per cent increase a year on the earlier total. In fact, as we shall see, the rate of increase was not as regular as that. All we have to consider, at this stage, is the percentage rise over a given period.</p>

<p>Can this rise in the total number of civil servants be accounted for except on the assumption that such a total must always rise by a law governing its growth? It might be urged at this point that the period under discussion was one of rapid development in naval technique.

<img alt="" src="01.b.svg" style="float: right" />

The use of the flying machine was no longer confined to the eccentric. Electrical devices were being multiplied and elaborated. Submarines were tolerated if not approved. Engineer officers were beginning to be regarded as almost human. In so revolutionary an age we might expect that storekeepers would have more elaborate inventories to compile. We might not wonder to see more draughtsmen on the payroll, more designers, more technicians and scientists.</p>

<p>But these, the dockyard officials, increased only by 40 per cent in number when the men of Whitehall increased their total by nearly 80 per cent. For every new foreman or electrical engineer at Portsmouth there had to be two more clerks at Charing Cross. From this we might be tempted to conclude, provisionally, that the rate of increase in administrative staff is likely to be double that of the technical staff at a time when the actually useful strength (in this case, of seamen) is being reduced by 31.5 per cent. It has been proved statistically, however, that this last percentage is irrelevant.</p>

<p>The officials would have multiplied at the same rate had there been no actual seamen at all.</p>

<p>It would be interesting to follow the further progress by which the 8118 Admiralty staff of 1935 came to number 33,788 by 1954. But the staff of the Colonial Office affords a better field of study during a period of imperial decline. Admiralty statistics are complicated by factors (like the Fleet Air Arm) that make comparison difficult as between one year and the next. The Colonial Office growth is more significant in that it is more purely administrative. Here the relevant statistics are as follows:</p>

<table>
<tr>
  <td>1935</td>
  <td>1939</td>
  <td>1943</td>
  <td>1947</td>
  <td>1954</td>
</tr>
<tr>
  <td>372</td>
  <td>450</td>
  <td>817</td>
  <td>1139</td>
  <td>1661</td>
</tr>
</table>

<p>Before showing what the rate of increase is, we must observe that the extent of this department’s responsibilities was far from constant during these twenty years. The colonial territories were not much altered in area or population between 1935 and 1939. They were considerably diminished by 1943, certain areas being in enemy hands. They were increased again in 1947, but have since then shrunk steadily from year to year as successive colonies achieve self-government. It would be rational to suppose that these changes in the scope of Empire would be reflected in the size of its central administration. But a glance at the figures is enough to convince us that the staff totals represent nothing but so many stages in an inevitable increase. And this increase, although related to that observed in other departments, has nothing to do with the size--or even the existence--of the Empire. What are the percentages of increase? We must ignore, for this purpose, the rapid increase in staff which accompanied the diminution of responsibility during World War II. We should note rather, the peacetime rates of increase: over 5.24 per cent between 1935 and 1939, and 6.55 per cent between 1947 and 1954. This gives an average increase of 5.89 per cent each year, a percentage markedly similar to that already found in the Admiralty staff increase between 1914 and 1928.</p>

<p>Further and detailed statistical analysis of departmental staffs would be inappropriate in such a work as this. It is hoped, however, to reach a tentative conclusion regarding the time likely to elapse between a given official’s first appointment and the later appointment of his two or more assistants.</p>

<p>Dealing with the problem of pure staff accumulation, all our researches so far completed point to an average increase of 5.75 per cent per year.</p>

<p>This fact established, it now becomes possible to state Parkinson’s Law in mathematical form: In any public administrative department not actually at war, the staff increase may be expected to follow this formula--</p>

<p class="formula"><img alt="" style="height: 3em" src="01.formula.1.svg" /></p>

<p><em>k</em> is the number of staff seeking promotion through the appointment of subordinates; <em>l</em> represents the difference between the ages of appointment and retirement; <em>m</em> is the number of man-hours devoted to answering minutes within the department; and <em>n</em> is the number of effective units being administered. <em>x</em> will be the number of new staff required each year.</p>

<p>Mathematicians will realize, of course, that to find the percentage increase they must multiply <em>x</em> by 100 and divide by the total of the previous year, thus:</p>

<p class="formula"><img alt="" style="height: 3em" src="01.formula.2.svg" /></p>

<p>where <em>y</em> represents the total original staff. This figure will invariably prove to be between 5.17 per cent and 6.56 per cent, irrespective of any variation in the amount of work (if any) to be done.</p>

<p>The discovery of this formula and of the general principles upon which it is based has, of course, no political value. No attempt has been made to inquire whether departments <em>ought</em> to grow in size. Those who hold that this growth is essential to gain full employment are fully entitled to their opinion. Those who doubt the stability of an economy based upon reading each other’s minutes are equally entitled to theirs. It would probably be premature to attempt at this stage any inquiry into the quantitative ratio that should exist between the administrators and the administered. Granted, however, that a maximum ratio exists, it should soon be possible to ascertain by formula how many years will elapse before that ratio, in any given community, will be reached. The forecasting of such a result will again have no political value. Nor can it be sufficiently emphasized that Parkinson’s Law is a purely scientific discovery, inapplicable except in theory to the politics of the day. It is not the business of the botanist to eradicate the weeds. Enough for him if he can tell us just how fast they grow.</p>

</body>
</html>
