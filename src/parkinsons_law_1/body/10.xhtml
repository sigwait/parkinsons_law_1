<?xml version="1.0" encoding="UTF-8"?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title/>
<link rel="stylesheet" href="../style.css" type="text/css"/>
</head>
<body>

<h1 class="title">Pension point, or the age of retirement</h1>
<p><span class="dropcap">Of the many</span> problems discussed and solved in this work, it is proper that the question of retirement should be left to the last. It has been the subject of many commissions of inquiry but the evidence heard has always been hopelessly conflicting and the final recommendations muddled, inconclusive, and vague. Ages of compulsory retirement are fixed at points varying from 55 to 75, all being equally arbitrary and unscientific.</p>

<p>Whatever age has been decreed by accident and custom can be defended by the same argument. Where the retirement age is fixed at 65 the defenders of this system will always have found, by experience, that the mental powers and energy show signs of flagging at the age of 62. This would be a most useful conclusion to have reached had not a different phenomenon been observed in organizations where the age of retirement has been fixed at 60. There, we are told, people are found to lose their grip, in some degree, at the age of 57. As against that, men whose retiring age is 55 are known to be past their best at 52. It would seem, in short, that efficiency declines at the age of R minus 3, irrespective of the age at which R has been fixed. This is an interesting fact in itself but not directly helpful when it comes to deciding what the R age is to be.</p>

<p>But while the R-3 age is not directly useful to us, it may serve to suggest that the investigations hitherto pursued have been on the wrong lines. The observation often made that men vary, some being old at 50, others still energetic at 80 or 90, may well be true, but here again the fact leads us nowhere. The truth is that the age of retirement should not be related in any way to the man whose retirement we are considering. It is his successor we have to watch: the man (Y) destined to replace the other man (X) when the latter retires. He will pass, as is well known, the following stages in his successful career:</p>

<ol>
  <li>Age of Qualification = Q</li>
  <li>Age of Discretion = D (Q + 3)</li>
  <li>Age of Promotion = P (D + 7)</li>
  <li>Age of Responsibility = R (P + 5)</li>
  <li>Age of Authority = A (R + 3)</li>
  <li>Age of Achievement = AA (A + 7)</li>
  <li>Age of Distinction = DD (AA + 9)</li>
  <li>Age of Dignity = DDD (DD + 6)</li>
  <li>Age of Wisdom = W (DDD + 3)</li>
  <li>Age of Obstruction = OO (W + 7)</li>
</ol>

<p>The above scale is governed by the numerical value of Q. Now, Q is to be understood as a technical term. It does not mean that a man at Q knows anything of the business he will have to transact. Architects, for example, pass some form of examination but are seldom found to know anything useful at that point (or indeed any other point) in their career. The term Q means the age at which a professional or business career begins, usually after an elaborate training that has proved profitable only to those paid for organizing it. It will be seen that if Q = 22, X will not reach OO (the Age of Obstruction) until he is 72. So far as his own efficiency is concerned, there is no valid reason for replacing him until he is 71. But our problem centers not on him but on Y, his destined successor. How are the ages of X and Y likely to compare? To be more exact, how old will X have been when Y first entered the department or firm?</p>

<p>This problem has been the subject of prolonged investigation. Our inquiries have tended to prove that the age gap between X and Y is exactly fifteen years. (It is not, we find, the normal practice for the son to succeed the father directly.) Taking this average of fifteen years, and assuming that Q = 22, we find that Y will have reached AA (the Age of Achievement) at 47, when X is only 62. And that, clearly, is where the crisis occurs. For Y, if thwarted in his ambition through X’s still retaining control, enters, it has been proved, a different series of stages in his career. These stages are as follows: </p>

<ol start="6">
  <li>Age of Frustration (F) = A + 7</li>
  <li>Age of Jealousy (J) = F + 9</li>
  <li>Age of Resignation (R) = J + 4</li>
  <li>Age of Oblivion (O) = R + 5</li>
</ol>

<p>When X, therefore, is 72, Y is 57, just entering on the Age of Resignation. Should X at last retire at that age, Y is quite unfit to take his place, being now resigned (after a decade of frustration and jealousy) to a career of mediocrity. For Y, opportunity will have come just ten years too late.</p>

<p>The age of Frustration will not always be the same in years, depending as it does on the factor Q, but its symptoms are easy to recognize. The man who is denied the opportunity of taking decisions of importance begins to

<img alt="" src="10.a.svg" />

regard as important the decisions he is allowed to take. He becomes fussy about filing, keen on seeing that pencils are sharpened, eager to ensure that the windows are open (or shut), and apt to use two or three different-colored inks. The Age of Jealousy reveals itself in an emphasis upon seniority. “After all, I am still somebody.” “I was never consulted.”</p>

<p>“Z has very little experience.” But that period gives place to the Age of Resignation. “I am not one of these ambitious types.” “Z is welcome to a seat on the Board--more trouble than it is worth, I should say.” “Promotion would only have interfered with my golf.” The theory has been advanced that the Age of Frustration is also marked by an interest in local politics. It is now known, however, that men enter local politics solely as a result of being unhappily married. It will be apparent, however, from the other symptoms described, that the man still in a subordinate position at 47 (or equivalent) will never be fit for anything else.</p>

<p>The problem, it is now clear, is to make X retire at the age of 60, while still able to do the work better than anyone else. The immediate change may be for the worse but the alternative is to have no possible successor at hand when X finally goes. And the more outstanding X has proved to be, and the longer his period of office, the more hopeless is the task of replacing him. Those nearest him in the seniority are already too old and have been subordinate for too long. All they can do is to block the way for anyone junior to them; a task in which they will certainly not fail. No competent successor will appear for years, nor at all until some crisis has brought a new leader to the fore. So the hard decision has to be taken.</p>

<p>Unless X goes in good time, the whole organization will eventually suffer.</p>

<p>But how is X to be moved?</p>

<p>In this, as in so many other matters, modern science is not at a loss.</p>

<p>The crude methods of the past have been superseded. In days gone by it was usual, no doubt, for the other directors to talk inaudibly at board meetings, one merely opening and shutting his mouth and another nodding in apparent comprehension, thus convincing the chairman that he was actually going deaf. But there is a modern technique that is far more effective and certain. The method depends essentially on air travel and the filling in of forms. Research has shown that complete exhaustion in modern life results from a combination of these two activities. The high official who is given enough of each will very soon begin to talk of retirement. It used to be the custom in primitive African tribes to liquidate the king or chief at a certain point in his career, either after a period of years or at the moment when his vital powers appeared to have gone. Nowadays the technique is to lay before the great man the program of a conference at Helsinki in June, a congress at Adelaide in July, and a convention at Ottawa in August, each lasting about three weeks. He is assured that the prestige of the department or firm will depend on his presence and that the delegation of this duty to anyone else would be regarded as an insult by all others taking part. The program of travel will allow of his return to the office for about three or four days between one conference and the next. He will find his in-tray piled high on each occasion with forms to fill in, some relating to his travels, some to do with applications for permits or quota allocations, and the rest headed “Income Tax.” On his completion of the forms awaiting his signature after the Ottawa convention, he will be given the program for a new series of conferences; one at Manila in September, the second at Mexico City in October, and the third at Quebec in November. By December he will admit that he is feeling his age. In January he will announce his intention to retire.</p>

<p>The essence of this technique is so to arrange matters that the conferences are held at places the maximum distance apart and in climates offering the sharpest contrast in heat and cold. There should be no possibility whatever of a restful sea voyage in any part of the schedule. It must be air travel all the way. No particular care need be taken in the choice between one route and another. All are alike in being planned for the convenience of the mails rather than the passengers. It can safely be assumed, almost without inquiry, that most flights will involve takeoff at 2.50 A.M., reporting at the airfield at 1.30 and weighing baggage at the terminal at 12.45. Arrival will be scheduled for 3.10 A.M. on the next day but one. The aircraft will invariably, however, be somewhat overdue, touching down in fact at 3.57 A.M., so that passengers will be clear of customs and immigration by about 4.35. Going one way around the world, it is possible and indeed customary to have breakfast about three times. In the opposite direction the passengers will have nothing to eat for hours at a stretch, being finally offered a glass of sherry when on the point of collapse from malnutrition. Most of the flight time will of course be spent in filling in various declarations about currency and health. How much have you in dollars (U.S.), pounds (sterling), francs, marks, guilders, yen, lire, and pounds (Australian); how much in letters of credit, travelers checks, postage stamps, and postal orders? Where did you sleep last night and the night before that? (This last is an easy question, for the air traveler is usually able to declare, in good faith, that he has not slept at all for the past week.) When were you born and what was your grandmother’s maiden name? How many children have you and why? What will be the length of your stay and where? What is the object of your visit, if any? (As if by now you could even remember.) Have you had chicken pox and why not? Have you a visa for Patagonia and a re-entry permit for Hongkong? The penalty for making a false declaration is life imprisonment. Fasten your seat belts, please. We are about to land at Rangoon. Local time is 2.47 A.M. Outside temperature is 110°F. We shall stop here for approximately one hour.</p>

<p>Breakfast will be served on the aircraft five hours after takeoff. Thank you. (For what, in heaven’s name?) No smoking, please.</p>

<p>It will be observed that air travel, considered as a retirement-accelerator, has the advantage of including a fair amount of form-filling. But form-filling proper is a separate ordeal, not necessarily connected with travel. The art of devising forms to be filled in depends on three elements: obscurity, lack of space, and the heaviest penalties for failure. In a form-compiling department, obscurity is ensured by various branches dealing respectively with ambiguity, irrelevance, and jargon. But some of the simpler devices have now become automatic. Thus, a favorite opening gambit is a section, usually in the top right-hand corner, worded thus:</p>

<table>
  <tr>
    <td>Return rendered in respect of the month of</td>
    <td>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</td>
  </tr>
</table>

<p>As you have been sent the form on February 16, you have no idea whether it relates to last month, this month or next. Only the sender knows that, but he is asking you. At this point the ambiguity expert takes over, collaborating closely with a space consultant, and this is the result:</p>

<table class="tbl-namecard">
  <tr style>
    <td style="width: 5em; text-align: center"><em>Cross out the word which does not apply</em></td>
    <td>Full name</td>
    <td>Address</td>
    <td>Domicile</td>
    <td>When naturalized and why</td>
    <td>Status</td>
  </tr>
  <tr>
    <td style="text-align: left;">
      Mr.<br/>
      Mrs.<br/>
      Miss
    </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>

<p>Such a form as this is especially designed, of course, for a Colonel, Lord, Professor, or Doctor called Alexander Winthrop Percival Blenkinsop-Fotheringay of Battleaxe Towers, Layer-de-la-Haye, near Newcastle-under-Lyme, Lincolnshire-parts-of-Kesteven (whatever that may mean). Follows the word “Domicile,” which is practically meaningless except to an international lawyer, and after that a mysterious reference to naturalization. Lastly, we have the word “Status,” which leaves the filler-in wondering whether to put “Admiral (Ret’d),” “Married,” “American Citizen” or “Managing Director.”</p>

<p>Now the ambiguity expert hands over the task to a specialist in irrelevance, who calls in a new space allocator to advise on layout:</p>

<table class="tbl-idcard">
  <tr>
    <td>Number of your identity card or passport</td>
    <td>Your grandfather’s full name</td>
    <td>Your grandmother’s maiden name</td>
    <td>Have you been vaccinated, inoculated; when &amp; why</td>
    <td>Give full details</td>
  </tr>
  <tr>
    <td>&#160;<br/>&#160;</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="5">
      <em>Note:</em> The penalty for furnishing incorrect information may be a fine of £5000 or a year’s penal servitude, or quite possibly both.
    </td>
  </tr>
</table>

<p>Then the half-completed work of art is sent to the jargon specialist, who produces something on these lines:</p>

<table>
  <tr>
    <td>What special circumstances<sup class="sup">283</sup> are alleged to justify the adjusted allocation for which request is made in respect of the quota period to which the former application<sup class="sup">143</sup> relates, whether or not the former level had been revised and in what sense and for what purpose and whether this or any previous application made by any other party or parties has been rejected by any other planning authority under subsection VII<sup class="sup">36</sup> or for any other reason, and whether this or the latter decision was made the subject of an appeal and with what result and why.</td>
    <td>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</td>
  </tr>
</table>

<p>Finally, the form goes to the technician, who adds the space-for-signature section, the finish that crowns the whole.</p>

<img alt="" src="10.b.svg" style="margin: 1em auto;" />

<p>This is quite straightforward except for the final touch of confusion as to whose photograph or thumb print is wanted, the I/we person or the witness. It probably does not matter, anyway.</p>

<p>Experiment has shown that an elderly man in a responsible position will soon be forced to retire if given sufficient air travel and sufficient forms. Instances are frequent, moreover, of such elderly men deciding to retire before the treatment has even begun. At the first mention of a conference at Stockholm or Vancouver, they often realize that their time has arrived. Very rarely nowadays is it necessary to adopt methods of a severe character. The last recorded resort to these was in a period soon after the conclusion of World War II. The high official concerned was particularly tough and the only remedy found was to send him on a tour of tin mines and rubber estates in Malaya. This method is best tried in January, and with jet aircraft to make the climatic transition more abrupt. On landing at 5.52 P.M. (Malayan time) this official was rushed off at once to a cocktail party, from that to another cocktail party (held at a house fifteen miles from the hotel where the first took place), and from that to a dinner party (eleven miles in the opposite direction). He was in bed by about 2.30 A.M. and on board an aircraft at seven the next morning. Landing at Ipoh in time for a belated breakfast, he was then taken to visit two rubber estates, a tin mine, an oil-palm plantation, and a factory for canning pineapples.</p>

<p>After lunch, given by the Rotary Club, he was taken to a school, a clinic, and a community center. There followed two cocktail parties and a Chinese banquet of twenty courses, the numerous toasts being drunk in neat brandy served in tumblers. The formal discussion on policy began next morning and lasted for three days, the meetings interspersed with formal receptions and nightly banquets in Sumatran or Indian style. That the treatment was too severe was fairly apparent by the fifth day, during the afternoon of which the distinguished visitor could walk only when supported by a secretary on one side, a personal assistant on the other. On the sixth day he died, thus confirming the general impression that he must have been tired or unwell. Such methods as these are now discountenanced, and have since indeed proved needless. People are learning to retire in time.</p>

<p>But a serious problem remains. What are we ourselves to do when nearing the retirement age we have fixed for others? It will be obvious at once that our own case is entirely different from any other case we have so far considered. We do not claim to be outstanding in any way, but it just so happens that there is no possible successor in sight. It is with genuine reluctance that we agree to postpone our retirement for a few years, purely in the public interest. And when a senior member of staff approaches us with details of a conference at Teheran or Hobart, we promptly wave it aside, announcing that all conferences are a waste of time. “Besides,” we continue blandly, “my arrangements are already made. I shall be salmon fishing for the next two months and will return to this office at the end of October, by which date I shall expect all the forms to have been filled in. Goodbye until then.” We knew how to make our predecessors retire. When it comes to forcing our own retirement, our successors must find some method of their own.</p>

</body>
</html>
