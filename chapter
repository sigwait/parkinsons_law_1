#!/usr/bin/env ruby

require 'nokogiri'
require 'erb'
require 'yaml'
require 'time'

raise "Usage: #{$0} meta.yaml input.html output.html" if ARGV.size < 3

def filename_to_chapter_num name
  (m = name.match(/\b(\d+)/)) ? m[1].to_i : nil
end

meta = YAML.load_file ARGV[0]
meta['modified'] = DateTime.now.to_time.utc.iso8601

xml = ERB.new(File.read ARGV[1]).result

doc = Nokogiri::XML xml
h1 = doc.at_css("h1.title")
if h1
  num = filename_to_chapter_num ARGV[1]

  if num && !h1.classes.index {|v| v == 'unnumbered'}
    name = "#{num}. #{h1.content}"
  else
    name = h1.content
  end

  doc.at_css('title').content = name
  h1.content = name
end

# inline SVGs
doc.css('p.mathjax').each do |node|
  IO.popen(['tex2svg', node.content]) do |f|
    svg = Nokogiri::XML f.read
    svg.root.append_class 'mathjax'
    node.inner_html = svg.inner_html
  end
end

path_prefix = File.dirname ARGV[2]
doc.css('p.formula > img').each do |node|
  svg = Nokogiri::XML File.read File.join path_prefix, node['src']
  node['alt'] = svg.css('title').text
end

File.open(ARGV[2], 'w') {|f| f.write doc.to_xml }
