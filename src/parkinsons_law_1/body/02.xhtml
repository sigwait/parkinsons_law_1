<?xml version="1.0" encoding="UTF-8"?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title/>
<link rel="stylesheet" href="../style.css" type="text/css"/>
</head>
<body>
<h1 class="title">The will of the people, or annual general meeting</h1>

<p><span class="dropcap">We are all</span> familiar with the basic difference between English and French parliamentary institutions; copied respectively by such other assemblies as derive from each. We all realize that this main difference has nothing to do with national temperament, but stems from their seating plans.</p>

<p>The British, being brought up on team games, enter their House of Commons in the spirit of those who would rather be doing something else. If they cannot be playing golf or tennis, they can at least pretend that politics is a game with very similar rules. But for this device, Parliament would arouse even less interest than it does. So the British instinct is to form two opposing teams, with referee and linesmen, and let them debate until they exhaust themselves. The House of Commons is so arranged that the individual Member is practically compelled to take one side or the other before he knows what the arguments are, or even (in some cases) before he knows the subject of the dispute. His training from birth has been to play for his side, and this saves him from any undue mental effort. Sliding into a seat toward the end of a speech, he knows exactly how to take up the argument from the point it has reached. If the speaker is on his own side of the House, he will say “Hear, hear!” If he is on the opposite side, he can safely say “Shame!” or merely “Oh!” At some later stage he may have time to ask his neighbor what the debate is supposed to be about. Strictly speaking, however, there is no need for him to do this. He knows enough in any case not to kick into his own goal. The men who sit opposite

<img alt="" src="02.a.svg" />

are entirely wrong and all their arguments are so much drivel. The men on his own side are statesmanlike, by contrast, and their speeches a singular blend of wisdom, eloquence, and moderation. Nor does it make the slightest difference whether he learned his politics at Harrow or in following the fortunes of Aston Villa. In either school he will have learned when to cheer and when to groan. But the British system depends entirely on its seating plan. If the benches did not face each other, no one could tell truth from falsehood--wisdom from folly--unless indeed by listening to it all. But to listen to it all would be ridiculous, for half the speeches must of necessity be nonsense.</p>

<p>In France the initial mistake was made of seating the representatives in a semicircle, all facing the chair. The resulting confusion could be imagined if it were not notorious. No real opposing teams could be formed and no one could tell (without listening) which argument was the more cogent. There was the further handicap of all the proceedings being in French--an example the United States wisely refused to follow. But the French system is bad enough even when the linguistic difficulty does not arise. Instead of having two sides, one in the right and the other in the wrong--so that the issue is clear from the outset--the French form a multitude of teams facing in all directions. With the field in such confusion, the game cannot even begin. Basically their representatives are of the Right or of the Left, according to where they sit. This is a perfectly sound scheme. The French have not gone to the extreme of seating people in alphabetical order. But the semicircular chamber allows of subtle distinctions between the various degrees of tightness and leftness. There is none of the clear-cut British distinction between rightness and wrongness.</p>

<p>One deputy is described, politically, as to the left of Monsieur Untel but well to the right of Monsieur Quelquechose. What is anyone to make of that?</p>

<p>What should we make of it even in English? What do they make of it themselves? The answer is, “Nothing.”</p>

<p>All this is generally known. What is less generally recognized is that the paramount importance of the seating plan applies to other assemblies and meetings, international, national, and local. It applies, moreover, to meetings round a table such as occur at a Round Table Conference. A moment’s thought will convince us that a Square Table Conference would be something totally different and a Long Table Conference would be different again.</p>

<p>These differences do not merely affect the length and acrimony of the discussion; they also affect what (if anything) is decided. Rarely, as we know, will the voting relate to the merits of the case. The final decision is influenced by a variety of factors, few of which need concern us at the moment. We should note, however, that the issue is actually <em>decided</em>, in the end, by the votes of the center bloc. This would not be true in the House of Commons, where no such bloc is allowed to develop. But at other conferences the center bloc is all important. This bloc essentially comprises the following elements:</p>

<ol style="list-style-type: lower-alpha;">
  <li>Those who have failed to master any one of the memoranda written in advance and showered weeks beforehand on all those who are expected to be present.</li>

  <li>Those who are too stupid to follow the proceedings at all. These are readily distinguishable by their tendency to mutter to each other: “What is the fellow talking about?”</li>

  <li>Those who are deaf. They sit with their hands cupping their ears, growling “I wish people would speak up.”</li>

  <li>Those who were dead drunk in the small hours and have turned up (heaven knows why) with a splitting headache and a conviction that nothing matters either way.</li>

  <li>The senile, whose chief pride is in being as fit as ever-fitter indeed than a lot of these younger men. “I <em>walked</em> here,” they whisper. “Pretty good for a man of eighty-two, what?”</li>

  <li>The feeble, who have weakly promised to support both sides and don’t know what to do about it. They are of two minds as to whether they should abstain from voting or pretend to be sick.</li>
</ol>

<p>Toward capturing the votes of the center bloc the first step is to identify and count the members. That done, everything else depends on where they are to sit. The best technique is to detail off known and stalwart supporters to enter into conversation with named middle-bloc types before the meeting actually begins. In this preliminary chat the stalwarts will carefully avoid mentioning the main subject of debate. They will be trained to use the opening gambits listed below, corresponding to the categories <em>a</em> to <em>f</em>, into which the middle bloc naturally falls: </p>

<ol style="list-style-type: lower-alpha;">
<li>“Waste of time, I call it, producing all these documents. I have thrown most of mine away.”</li>

<li>“I expect we shall be dazzled by eloquence before long. I often wish people would talk less and come to the point. They are too clever by half, if you ask me.”</li>

<li>“The acoustics of this hall are simply terrible. You would have thought these scientific chaps could do something about it. For half the time I CAN’T HEAR WHAT IS BEING SAID. CAN YOU?”</li>

<li>“What a rotten place to meet! I think there is something the matter with the ventilation. It makes me feel almost unwell. What about you?”</li>

<li>“My goodness, I don’t know how you do it! Tell me the secret. Is it what you have for breakfast?”</li>

<li>“There’s so much to be said on both sides of the question that I really don’t know which side to support. What do you feel about it?”</li>
</ol>

<p>If these gambits are correctly played, each stalwart will start a lively conversation, in the midst of which he will steer his middle-blocsman toward the forum. As he does this, another stalwart will place himself just <em>ahead</em> of the pair and moving in the same direction. The drill is best illustrated by a concrete example. We will suppose that stalwart X (Mr. Sturdy) is steering middle-blocsman Y (Mr. Waverley, type <em>f</em>) toward a seat <em>near the front</em>. Ahead goes stalwart Z (Mr. Staunch), who presently takes a seat without appearing to notice the two men following him. Staunch turns in the opposite direction and waves to someone in the distance. Then he leans over to make a few remarks to the man in front of him. Only when Waverley has sat down will Staunch presently turn toward him and say, “My dear fellow--how nice to see you!” Only some minutes later again will he catch sight of Sturdy and start visibly with surprise. “Hallo, Sturdy--I didn’t think you would be here!” “I’ve recovered now,” replies Sturdy. “It was only a chill.” The seating order is thus made to appear completely accidental, casual, and friendly. That completes Phase I of the operation, and it would be much the same whatever the exact category in which the middle-blocsman is believed to fall.</p>

<p>Phase II has to be adjusted according to the character of the man to be influenced. In the case of Waverley (Type <em>f</em>) the object in Phase II is to avoid any discussion of the matter at issue but to produce the impression that the thing is already decided. Seated near the front, Waverley will be unable to see much of the other members and can be given the impression that they practically all think alike.</p>

<p>“Really,” says Sturdy, “I don’t know why I bothered to come. I gather that Item Four is pretty well agreed. All the fellows I meet seem to have made up their minds to vote for it.” (Or against it, as the case may be.) “Curious,” says Staunch. “I was just going to say the same thing. The issue hardly seems to be in doubt.”</p>

<p>“I had not really made up my own mind,” says Sturdy.

<img alt="" src="02.b.svg" />

“There was much to be said on either side. But opposition would really be a waste of time.</p>

<p>What do you think, Waverley?”</p>

<p>“Well,” says Waverley, “I must admit that I find the question rather baffling. On the one hand, there is good reason to agree to the motion …</p>

<p>As against that… Do you think it will pass?”</p>

<p>“My dear Waverley, I would trust your judgment in this. You were saying just now that it is already agreed.”</p>

<p>“Oh, was I? Well, there does seem to be a majority. … Or perhaps I should say …”</p>

<p>“Thank you, Waverley,” says Staunch, “for your opinion. I think just the same but am particularly interested to find you agree with me. There is no one whose opinion I value more.”</p>

<p>Sturdy, meanwhile, is leaning over to talk to someone in the row behind. What he actually says, in a low voice, is this, “How is your wife now? Is she out of hospital?” When he turns back again, however, it is to announce that the people behind all think the same. The motion is as good as passed. And so it is if the drill goes according to plan.</p>

<p>While the other side has been busy preparing speeches and phrasing amendments, the side with the superior technique will have concentrated on pinning each middle-blocsman between two reliable supporters. When the crucial moment comes, the raising of a hand on either side will practically compel the waverer to follow suit. Should he be actually asleep, as often happens with middle-blocsman in categories <em>d</em> and <em>e</em>, his hand will be raised for him by the member on his right. This rule is merely to obviate both his hands being raised, a gesture that has been known to attract unfavorable comment. With the middle bloc thus secured, the motion will be carried with a comfortable margin; or else rejected, if that is thought preferable. In nearly every matter of controversy to be decided by the will of the people, we can assume that the people who will decide are members of the middle bloc. Delivery of speeches is therefore a waste of time. The one party will never agree and the other party has agreed already. Remains the middle bloc, the members of which divide into those who cannot hear what is being said and those who would not understand it even if they did. To secure their votes what is needed is primarily the example of others voting on either side of them. Their votes can thus be swayed by accident. How much better, by contrast, to sway them by design!</p>

</body>
</html>
