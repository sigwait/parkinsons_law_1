<?xml version="1.0" encoding="UTF-8"?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title/>
<link rel="stylesheet" href="../style.css" type="text/css"/>
</head>
<body>

<h1 class="title">Directors and councils, or coefficient of inefficiency</h1>

<p><span class="dropcap">The life cycle</span> of the committee is so basic to our knowledge of current affairs that it is surprising more attention has not been paid to the science of comitology. The first and most elementary principle of this science is that a committee is organic rather than mechanical in its nature: it is not a structure but a plant. It takes root and grows, it flowers, wilts, and dies, scattering the seed from which other committees will bloom in their turn. Only those who bear this principle in mind can make real headway in understanding the structure and history of modern government.</p>

<p>Committees, it is nowadays accepted, fall broadly into two categories, those (a) from which the individual member has something to gain; and those (b) to which the individual member merely has something to contribute.</p>

<p>Examples of the B group, however, are relatively unimportant for our purpose; indeed some people doubt whether they are committees at all. It is from the more robust A group that we can learn most readily the principles which are common (with modifications) to all. Of the A group the most deeply rooted and luxuriant committees are those which confer the most power and prestige upon their members. In most parts of the world these committees are called “cabinets.” This chapter is based on an extensive study of national cabinets, over space and time.</p>

<p>When first examined under the microscope, the cabinet council usually appears--to comitologists, historians, and even to the people who appoint cabinets--to consist ideally of five. With that number the plant is viable, allowing for two members to be absent or sick at any one time. Five members are easy to collect and, when collected, can act

<img alt="" src="04.a.svg" />

with competence, secrecy, and speed. Of these original members four may well be versed, respectively, in finance, foreign policy, defense, and law. The fifth, who has failed to master any of these subjects, usually becomes the chairman or prime minister.</p>

<p>Whatever the apparent convenience might be of restricting the membership to five, however, we discover by observation that the total number soon rises to seven or nine. The usual excuse given for this increase, which is almost invariable (exceptions being found, however, in Luxembourg and Honduras), is the need for special knowledge on more than four topics. In fact, however, there is another and more potent reason for adding to the team. For in a cabinet of nine it will be found that policy is made by three, information supplied by two, and financial warning uttered by one. With the neutral chairman, that accounts for seven, the other two appearing at first glance to be merely ornamental. This allocation of duties was first noted in Britain in about 1639, but there can be no doubt that the folly of including more than three able and talkative men in one committee had been discovered long before then. We know little as yet about the function of the two silent members but we have good reason to believe that a cabinet, in this second stage of development, might be unworkable without them.</p>

<p>There are cabinets in the world (those of Costa Rica, Ecuador, Northern Ireland, Liberia, the Philippines, Uruguay, and Panama will at once be called to mind) which have remained in this second stage--that is, have restricted their membership to nine. These remain, however, a small minority. Elsewhere and in larger territories cabinets have generally been subject to a law of growth. Other members come to be admitted, some with a claim to special knowledge but more because of their nuisance value when excluded. Their opposition can be silenced only by implicating them in every decision that is made. As they are brought in (and placated) one after another, the total membership rises from ten toward twenty. In this third stage of cabinets, there are already considerable drawbacks.</p>

<p>The most immediately obvious of these disadvantages is the difficulty of assembling people at the same place, date, and time. One member is going away on the 18th, whereas another does not return until the 21st. A third is never free on Tuesdays, and a fourth never available before 5 P.M. But that is only the beginning of the trouble, for, once most of them are collected, there is a far greater chance of members proving to be elderly, tiresome, inaudible, and deaf. Relatively few were chosen from any idea that they are or could be or have ever been useful. A majority perhaps were brought in merely to conciliate some outside group. Their tendency is therefore to report what happens to the group they represent. All secrecy is lost and, worst of all, members begin to prepare their speeches. They address the meeting and tell their friends afterwards about what they imagine they have said. But the more these merely representative members assert themselves, the more loudly do other outside groups clamor for representation. Internal parties form and seek to gain strength by further recruitment. The total of twenty is reached and passed. And thereby, quite suddenly, the cabinet enters the fourth and final stage of its history.</p>

<p>For at this point of cabinet development (between 20 and 22 members) the whole committee suffers an abrupt organic or chemical change. The nature of this change is easy to trace and comprehend. In the first place, the five members who matter will have taken to meeting beforehand. With decisions already reached, little remains for the nominal executive to do. And, as a consequence of this, all resistance to the committee’s expansion comes to an end. More members will not waste more time; for the whole meeting is, in any case, a waste of time. So the pressure of outside groups is temporarily satisfied by the admission of their representatives, and decades may elapse before they realize how illusory their gain has been. With the doors wide open, membership rises from 20 to 30, from 30 to 40. There may soon be an instance of such a membership reaching the thousand mark. But this does not matter. For the cabinet has already ceased to be a real cabinet, and has been succeeded in its old functions by some other body.</p>

<p>Five times in English history the plant has moved through its life cycle. It would admittedly be difficult to prove that the first incarnation of the cabinet--the English Council of the Crown, now called the House of Lords--ever had a membership as small as five. When we first hear of it, indeed, its more intimate character had already been lost, with a hereditary membership varying from 29 to 50. Its subsequent expansion, however, kept pace with its loss of power. In round figures, it had 60 members in 1601, 140 in 1661, 220 in 1760, 400 in 1850, 650 in 1911, and 850 in 1952.</p>

<p>At what point in this progression did the inner committee appear in the womb of the peerage? It appeared in about 1257, its members being called the Lords of the King’s Council and numbering less than 10. They numbered no more than 11 in 1378, and as few still in 1410. Then, from the reign of Henry V, they began to multiply. The 20 of 1433 had become the 41 of 1504, the total reaching 172 before the council finally ceased to meet.</p>

<p>Within the King’s Council there developed the cabinet’s third incarnation--the Privy Council--with an original membership of nine. It rose to 20 in 1540, to 29 in 1547, and to 44 in 1558. The Privy Council as it ceased to be effective increased proportionately in size. It had 47 members in 1679, 67 in 1723, 200 in 1902, and 300 in 1951.</p>

<p>Within the Privy Council there developed the junto or Cabinet Council, which effectively superseded the former in about 1615. Numbering 8 when we first hear of it, its members had come to number 12 by about 1700, and 20 by 1725. The Cabinet Council was then superseded in about 1740 by an inner group, since called simply the Cabinet. Its development is best studied in tabular form. This is shown in Table I.</p>

<table style="width: 15em">
<caption>Table I -- growth of the english cabinet</caption>
<tr><td>1740</td><td>5</td></tr>
<tr><td>1784</td><td>7</td></tr>
<tr><td>1801</td><td>12</td></tr>
<tr><td>1841</td><td>14</td></tr>
<tr><td>1885</td><td>16</td></tr>
<tr><td>1900</td><td>20</td></tr>
<tr><td>1915</td><td>22</td></tr>
<tr><td>1935</td><td>22</td></tr>
<tr><td>1939</td><td>23</td></tr>
<tr><td>1945</td><td>16</td></tr>
<tr><td>1945</td><td>20</td></tr>
<tr><td>1949</td><td>17</td></tr>
<tr><td>1954</td><td>18</td></tr>
</table>

<p>From 1939, it will be apparent, there has been a struggle to save this institution; a struggle similar to the attempts made to save the Privy Council during the reign of Queen Elizabeth I. The Cabinet appeared to be in its decline in 1940, with an inner cabinet (of 5, 7, or 9 members) ready to take its place. The issue, however, remains in doubt. It is just possible that the British cabinet is still an important body.</p>

<p>Compared with the cabinet of Britain, the cabinet of the United States has shown an extraordinary resistance to political inflation. It had the appropriate number of 5 members in 1789, still only 7 by 1840, 9 by 1901, 10 by 1913, 11 by 1945, and then--against tradition--had come down to 10 again by 1953. Whether this attempt, begun in 1947, to restrict the membership will succeed for long is doubtful. All experience would suggest the inevitability of the previous trend. In the meanwhile, the United States enjoys (with Guatemala and El Salvador) a reputation for cabinet-exclusiveness, having actually fewer cabinet ministers than Nicaragua or Paraguay.</p>

<table style="min-width: 15em">
<caption>Table II -- size of cabinets</caption>
<tr><th>No. of Members</th><th></th></tr>
<tr><td>6</td><td>Honduras, Luxembourg</td></tr>
<tr><td>7</td><td>Haiti, Iceland, Switzerland</td></tr>
<tr><td>9</td><td>Costa Rica, Ecuador, N. Ireland, Liberia, Panama, Philippines, Uruguay</td></tr>
<tr><td>10</td><td>Guatemala, El Salvador, United States</td></tr>
<tr><td>11</td><td>Brazil, Nicaragua, Pakistan, Paraguay</td></tr>
<tr><td>12</td><td>Bolivia, Chile, Peru</td></tr>
<tr><td>13</td><td>Colombia, Dominican R., Norway, Thailand</td></tr>
<tr><td>14</td><td>Denmark, India, S. Africa, Sweden</td></tr>
<tr><td>15</td><td>Austria, Belgium, Finland, Iran, New Zealand, Portugal, Venezuela</td></tr>
<tr><td>16</td><td>Iraq, Netherlands, Turkey</td></tr>
<tr><td>17</td><td>Eire, Israel, Spain</td></tr>
<tr><td>18</td><td>Egypt, Gt. Britain, Mexico</td></tr>
<tr><td>19</td><td>W. Germany, Greece, Indonesia, Italy</td></tr>
<tr><td>20</td><td>Australia, Formosa, Japan</td></tr>
<tr><td>21</td><td>Argentina, Burma, Canada, France</td></tr>
<tr><td>22</td><td>China</td></tr>
<tr><td>24</td><td>E. Germany</td></tr>
<tr><td>26</td><td>Bulgaria</td></tr>
<tr><td>27</td><td>Cuba</td></tr>
<tr><td>29</td><td>Rumania</td></tr>
<tr><td>32</td><td>Czechoslovakia</td></tr>
<tr><td>35</td><td>Yugoslavia</td></tr>
<tr><td>38</td><td>USSR</td></tr>
</table>

<p>How do other countries compare in this respect? The majority of non-totalitarian countries have cabinets that number between 12 and 20 members. Taking the average of over 60 countries, we find that it comes to over 16; the most popular numbers are 15 (seven instances) and 9 (seven again). Easily the queerest cabinet is that of New Zealand, one member of which has to be announced as “Minister of Lands, Minister of Forests, Minister of Maori Affairs, Minister in charge of Maori Trust Office and of Scenery Preservation.” The toastmaster at a New Zealand banquet must be equally ready to crave silence for “The Minister of Health, Minister Assistant to the Prime Minister, Minister in Charge of State Advances Corporation, Census, and Statistics Department, Public Trust Office and Publicity and Information.” In other lands this oriental profusion is fortunately rare.</p>

<p>A study of the British example would suggest that the point of ineffectiveness in a cabinet is reached when the total membership exceeds 20 or perhaps 21. The Council of the Crown, the King’s Council, the Privy Council had each passed the 20 mark when their decline began. The present British cabinet is just short of that number now, having recoiled from the abyss. We might be tempted to conclude from this that cabinets--or other committees--with a membership in excess of 21 are losing the reality of power and that those with a larger membership have already lost it. No such theory can be tenable, however, without statistical proof. Table II on the preceding page attempts to furnish part of it.</p>

<p>Should we be justified in drawing a line in that table under the name of France (21 cabinet members) with an explanatory note to say that the cabinet is not the real power in countries shown below that line? Some comitologists would accept that conclusion without further research.</p>

<p>Others emphasize the need for careful investigation, more especially around the borderline of 21. But that the coefficient of inefficiency must lie between 19 and 22 is now very generally agreed.</p>

<p>What tentative explanation can we offer for this hypothesis? Here we must distinguish sharply between fact and theory, between the symptom and the disease. About the most obvious symptom there is little disagreement. It is known that with over 20 members present a meeting begins to change character. Conversations develop separately at either end of the table. To make himself heard, the member has therefore to rise. Once on his feet, he cannot help making a speech, if only from force of habit. “Mr. Chairman,” he will begin, “I think I may assert without fear of contradiction--and I am speaking now from twenty-five (I might almost say twenty-seven) years of experience--that we must view this matter in the gravest light. A heavy responsibility rests upon us, sir, and I for one…” Amid all this drivel the useful men present, if there are any, exchange little notes that read, “Lunch with me tomorrow--we’ll fix it then.”</p>

<p>What else can they do? The voice drones on interminably. The orator might just as well be talking in his sleep. The committee of which he is the most useless member has ceased to matter. It is finished. It is hopeless. It is dead.</p>

<p>So much is certain. But the root cause of the trouble goes deeper and has still, in part, to be explored. Too many vital factors are unknown. What is the shape and size of the table? What is the average age of those present? At what hour does the committee meet? In a book for the non-specialist it would be absurd to repeat the calculations by which the first and tentative coefficient of inefficiency has been reached. It should be enough to state that prolonged research at the Institute of Comitology has given rise to a formula which is now widely (although not universally) accepted by the experts in this field. It should perhaps be explained that the investigators assumed a temperate climate, leather-padded chairs and a high level of sobriety. On this basis, the formula is as follows:</p>

<p class="formula"><img alt="" style="height: 3em" src="04.formula.1.svg" /></p>

<p>Where m = the average number of members actually present; <em>o</em> = the number of members influenced by outside pressure groups; a = the average age of the members; d = the distance in centimeters between the two members who are seated farthest from each other; y = the number of years since the cabinet or committee was first formed; p = the patience of the chairman, as measured on the Peabody scale; b = the average blood pressure of the three oldest members, taken shortly before the time of meeting. Then x = the number of members effectively present at the moment when the efficient working of the cabinet or other committee has become manifestly impossible. This is the coefficient of inefficiency and it is found to lie between 19.9 and 22.4. (The decimals represent partial attendance; those absent for a part of the meeting.)</p>

<p>It would be unsound to conclude, from a cursory inspection of this equation that the science of comitology is in an advanced state of development. Comitologists and subcomitologists would make no such claim, if only from fear of unemployment. They emphasize, rather, that their studies have barely begun and that they are on the brink of astounding progress. Making every allowance for self-interest--which means discounting 90 per cent of what they say--we can safely assume that much work remains to do.</p>

<p>We should eventually be able, for example, to learn the formula by which the optimum number of committee members may be determined. Somewhere between the number of 3 (when a quorum is impossible to collect) and approximately 21 (when the whole organism begins to perish), there lies the golden number. The interesting theory has been propounded that this number must be 8. Why? Because it is the only number which all existing states (See Table II above) have agreed to avoid. Attractive as this theory may seem at first sight, it is open to one serious objection. Eight was the number preferred by King Charles I for his Committee of State. And look what happened to him!</p>

<img alt="" src="04.b.svg" style="margin-top: 1em" />

</body>
</html>
